const languageModal = require("../../models/language");
const multer = require("multer");
const path = require("path");
const root = process.cwd();
const imageFilter = require("../../config/imageFilter");
const fs = require("fs");

class languagesController {
    static datatable_data = async (req, res) => {
        try {
            const params = req.body;
            const query = datatablesQuery(languageModal);
            const raw = await query.run(params);
            return res.send(raw);
        } catch (error) {
            console.log(error);
            return res
                .status(500)
                .send("Something went wrong please try again later");
        }
    };

    static list = async (req, res) => {
        try {
            let sliders = await languageModal.find().sort({
                created_at: -1,
            });
            return res.render("admin/languages", {
                sliders,
            });
        } catch (error) {
            return res
                .status(500)
                .send("Something went wrong please try again later");
        }
    };

    static add = async (req, res) => {
        try {
           
            const slider = languageModal({
                name: req.body.name,
                description: req.body.description,
            });
            await slider.save();
            return res.send("success");
        } catch (error) {
            console.log(error);
            return res
                .status(500)
                .send("Somthing went wrong please try again later");
        }
    };

    static delete = async (req, res) => {
        try {
            const slider = await languageModal.findOne({
                _id: req.body.id,
            });
            
            await slider.deleteOne({
                _id: slider.id,
            });
           
            return res.send("success");
        } catch (error) {
            console.log(error)
            return res
                .status(500)
                .send("Something went wrong please try again later");
        }
    };
}

// Set The Storage Engine
const storage = multer.diskStorage({
    destination: path.join(root, "/public/dist/languages"),
    filename: function (req, file, cb) {
        cb(null, `${Date.now()}.jpg`);
    },
});

// Init Upload
const upload = multer({
    storage: storage,
    // limits: {
    //     fileSize: 5000000
    // },
    fileFilter: imageFilter,
}).single("image");

module.exports = languagesController;
