const Subscription = require("../../models/Subscription");
const Doctor = require("../../models/Doctor");
const multer = require("multer");
const path = require("path");
const root = process.cwd();
const imageFilter = require("../../config/imageFilter");
const fs = require("fs");
const datatablesQuery = require("datatables-query");

class subscriptionController {
    static datatable_data = async (req, res) => {
        try {
            const params = req.body;
            const query = datatablesQuery(Subscription);
            const raw = await query.run(params);
            return res.send(raw);
        } catch (error) {
            console.log(error);
            return res
                .status(500)
                .send("Something went wrong please try again later");
        }
    };

    static list = (req, res) => {
        try {
            return res.render("admin/subscription-list");
        } catch (error) {
            return res.send("Something went wrong please try again later");
        }
    };

    static add = async (req, res) => {
        try {
            
            if(req.body.month == ''){
                return res.send("Month is required");
            }
            if(req.body.sessions == ''){
                return res.send("Coins is required");
            }
            if(req.body.amount == ''){
                return res.send("Amount is required");
            }
            if(req.body.original_amount == ''){
                return res.send("Amount is required");
            }
            const category = Subscription({
                month: Number(req.body.month),
                sessions: Number(req.body.sessions),
                amount: Number(req.body.amount),
                original_amount: Number(req.body.original_amount),
            });
            await category.save();
            return res.send("Subscription plan added successfully");
        } catch (error) {
            console.log(error);
            return res
                .status(500)
                .send("Somthing went wrong please try again later");
        }
    };

    static edit = async (req, res) => {
        try {
            if(req.body.month == ''){
                return res.send("Month is required");
            }
            if(req.body.sessions == ''){
                return res.send("Sessions is required");
            }
            if(req.body.amount == ''){
                return res.send("Amount is required");
            }
            if(req.body.original_amount == ''){
                return res.send("Original Amount is required");
            }

            const category = await Subscription.findOne({
                _id: req.body.editid,
            });

            await Subscription.findOneAndUpdate(
                {
                    _id: req.body.editid,
                },
                {
                    month: Number(req.body.month),
                    sessions: Number(req.body.sessions),
                    amount: Number(req.body.amount),
                    original_amount: Number(req.body.original_amount),
                    updated_at: Date.now(),
                }
            );
            return res.send("Subscription plan updated successfully");
        } catch (error) {
            console.log(error);
            return res
                .status(500)
                .send("Somthing went wrong please try again later");
        }
    };

    static delete = async (req, res) => {
        try {
            const category = await Subscription.findOne({
                _id: req.body.id,
            });
           // const doctor = await Doctor.findOne({ category: category._id });
            // if (doctor)
            //     return res
            //         .status(500)
            //         .send("Catgegory is in use. You can't delete it");
            // const path = `${root}/public/dist/dci/${category.icon}`;
            // if (fs.existsSync(path)) fs.unlinkSync(path);
            await Subscription.deleteOne({
                _id: category.id,
            });
            return res.send("Plan deleted successfully");
        } catch (error) {
            console.log(error);
            return res
                .status(500)
                .send("Something went wrong please try again later");
        }
    };
}

// Set The Storage Engine
const storage = multer.diskStorage({
    destination: path.join(root, "/public/dist/dci"),
    filename: function (req, file, cb) {
        cb(null, `${Date.now()}.jpg`);
    },
});

// Init Upload
const upload = multer({
    storage: storage,
    limits: {
        fileSize: 5000000,
    },
    fileFilter: imageFilter,
}).single("icon");

// Init Upload
const editupload = multer({
    storage: storage,
    limits: {
        fileSize: 5000000,
    },
    fileFilter: imageFilter,
}).single("editicon");

module.exports = subscriptionController;
