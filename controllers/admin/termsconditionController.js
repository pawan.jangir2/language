const TermsCondition = require("../../models/TermsCondition");

class TermsConditionController {
    static termsconditionGET = async (req, res) => {
        try {
            const data = await TermsCondition.findOne({});

            return res.render("admin/termscondition", {
                content: data ? data.content : "",
            });
        } catch (error) {
            console.log(error);
            return res.send("Something went wrong please try again later");
        }
    };

    static termsconditionPOST = async (req, res) => {
        try {
            let data = req.body;
            data.updated_at = Date.now();
            const termscondition = TermsCondition(data);
            await termscondition.save();
            return res.send("Terms Condition updated successfully");
        } catch (error) {
            console.log(error);
            return res
                .status(500)
                .send("Something went wrong please try again later");
        }
    };
}

module.exports = TermsConditionController;
