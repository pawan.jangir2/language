const FreeVideos = require("../../models/FreeVideos");
const multer = require("multer");
const path = require("path");
const root = process.cwd();
const imageFilter = require("../../config/imageFilter");
const fs = require("fs");

class freevideosController {
    static datatable_data = async (req, res) => {
        try {
            const params = req.body;
            const query = datatablesQuery(FreeVideos);
            const raw = await query.run(params);
            return res.send(raw);
        } catch (error) {
            console.log(error);
            return res
                .status(500)
                .send("Something went wrong please try again later");
        }
    };

    static list = async (req, res) => {
        try {
            let sliders = await FreeVideos.find().sort({
                created_at: -1,
            });
            return res.render("admin/free_videos", {
                sliders,
            });
        } catch (error) {
            return res
                .status(500)
                .send("Something went wrong please try again later");
        }
    };

    static add = async (req, res) => {
        try {
           
            upload(req, res, async function (err) {
                if (req.fileValidationError) {
                    return res.send(req.fileValidationError);
                } else if (!req.file) {
                    return res.send("Please upload an image");
                } else if (err instanceof multer.MulterError) {
                    console.log(err);
                    return res.send(err);
                } else if (err) {
                    console.log(err);
                    return res.send(err);
                }
                const slider = FreeVideos({
                    icon: req.file.filename,
                    name: req.body.name,
                    description: req.body.description,
                    link: req.body.link,
                });
                await slider.save();
                return res.send("success");
            });
        } catch (error) {
            console.log(error);
            return res
                .status(500)
                .send("Somthing went wrong please try again later");
        }
    };

    static delete = async (req, res) => {
        try {
            const slider = await FreeVideos.findOne({
                _id: req.body.id,
            });
            
            await slider.deleteOne({
                _id: slider.id,
            });
           
            return res.send("success");
        } catch (error) {
            console.log(error)
            return res
                .status(500)
                .send("Something went wrong please try again later");
        }
    };
}

// Set The Storage Engine
const storage = multer.diskStorage({
    destination: path.join(root, "/public/dist/free_videos"),
    filename: function (req, file, cb) {
        cb(null, `${Date.now()}.jpg`);
    },
});

// Init Upload
const upload = multer({
    storage: storage,
    // limits: {
    //     fileSize: 5000000
    // },
    fileFilter: imageFilter,
}).single("image");

module.exports = freevideosController;
