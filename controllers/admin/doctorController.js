const Doctor = require("../../models/Doctor");
const DoctorCategory = require("../../models/DoctorCategory");
const multer = require("multer");
const path = require("path");
const root = process.cwd();
const imageFilter = require("../../config/imageFilter");
const datatablesQuery = require("datatables-query");

class DoctorController {
    static datatable_data = async (req, res) => {
        try {
            const params = req.body;
            const query = datatablesQuery(Doctor, [
                { path: "category", select: "name" },
            ]);
            const raw = await query.run(params);
            return res.send(raw);
        } catch (error) {
            console.log(error);
            return res
                .status(500)
                .send("Something went wrong please try again later");
        }
    };

    static list = (req, res) => {
        try {
            return res.render("admin/doctor-list");
        } catch (error) {
            return res.send("Something went wrong please try again later");
        }
    };

    static addGET = async (req, res) => {
        try {
            const categories = await DoctorCategory.find().sort({
                created_at: -1,
            });
            return res.render("admin/add-doctor", {
                categories,
            });
        } catch (error) {
            console.log(error);
            return res.send("Something went wrong please try again later");
        }
    };

    static addPOST = async (req, res) => {
        try {
            upload(req, res, async function (err) {
                if (req.fileValidationError) {
                    return res.send(req.fileValidationError);
                } else if (!req.file) {
                    return res.send("Please upload an image");
                } else if (err instanceof multer.MulterError) {
                    console.log(err);
                    return res.send(err);
                } else if (err) {
                    console.log(err);
                    return res.send(err);
                }

                let data = req.body;
                data.name = data.name.toLowerCase();
                data.location = data.location.toLowerCase();
                data.location = data.experties.toLowerCase();
                data.language = data.language.toLowerCase();
                data.account_holder = data.account_holder.toLowerCase();
                data.bank_name = data.bank_name.toLowerCase();
                data.image = req.file.filename;
                const doctor = Doctor(data);
                await doctor.save();
                return res.send("Doctor added successfully");
            });
        } catch (error) {
            console.log(error);
            return res
                .status(500)
                .send("Something went wrong please try again later");
        }
    };

    static editGET = async (req, res) => {
        try {
            const categories = await DoctorCategory.find().sort({
                created_at: -1,
            });
            const doctor = await Doctor.findOne({
                _id: req.query.id,
            });
            !doctor
                ? res.send("Doctor not found")
                : res.render("admin/edit-doctor", {
                      categories,
                      doctor,
                  });
        } catch (error) {
            return res.send("Something went wrong please try again later");
        }
    };

    static editPOST = async (req, res) => {
        try {
            let data = req.body;
            await Doctor.updateOne(
                {
                    _id: req.body.id,
                },
                data
            );
            return res.send("Doctor updated successfully");
        } catch (error) {
            console.log(error);
            return res
                .status(500)
                .send("Something went wrong please try again later");
        }
    };

    static delete = async (req, res) => {
        try {
            await Doctor.deleteOne({
                _id: req.body.id,
            });
            return res.send("Doctor deleted successfully");
        } catch (error) {
            console.log(error);
            return res
                .status(500)
                .send("Something went wrong please try again later");
        }
    };
}

// Set The Storage Engine
const storage = multer.diskStorage({
    destination: path.join(root, "/public/dist/di"),
    filename: function (req, file, cb) {
        cb(null, `${Date.now()}.jpg`);
    },
});

// Init Upload
const upload = multer({
    storage: storage,
    limits: {
        fileSize: 5000000,
    },
    fileFilter: imageFilter,
}).single("image");

const fixCategory = (data) => {
    return new Promise(async (resolve, reject) => {
        // data[0].category_id = "wioji";
        for (var i in data) {
            try {
                let category = await DoctorCategory.findById(
                    data[i].category_id
                );
                data[i].category_name = category.name;
            } catch (error) {
                console.log(error);
            }
        }
        resolve(data);
    });
};

module.exports = DoctorController;
