const Adminauth = require("../../models/Adminauth");
const bcrypt = require("bcrypt");

class AuthController {
    static loginGET = async (req, res) => {
        return res.render("admin/login");
    };

    static loginPOST = async (req, res) => {
        const username = req.body.username;
        const password = req.body.password;
        if (!username || !password)
            return res.send("Something went wrong please try again later");
        const user = await Adminauth.findOne({
            username: username,
        });
        if (!user) return res.send("Account not found");
        const validPassword = await bcrypt.compare(password, user.password);
        if (!validPassword) return res.status(500).send("Invalid Password");
        req.session.username = user.username;
        req.session.password = user.password;
        if (req.session.path) {
            return res.send(req.session.path);
        } else {
            return res.send("success");
        }
    };

    static changepasswordGET = async (req, res) => {
        return res.render("admin/changepassword");
    };

    static logout = async (req, res) => {
        try {
            req.session.destroy();
            return res.send("success");
        } catch (error) {
            return res.send("Something went wrong please try again later");
        }
    };
}

module.exports = AuthController;
