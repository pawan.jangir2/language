const PrivacyPolicy = require("../../models/PrivacyPolicy");

class PrivacyPolicyController {
    static privacypolicyGET = async (req, res) => {
        try {
            const data = await PrivacyPolicy.findOne({});

            return res.render("admin/privacypolicy", {
                content: data ? data.content : "",
            });
        } catch (error) {
            console.log(error);
            return res.send("Something went wrong please try again later");
        }
    };

    static privacypolicyPOST = async (req, res) => {
        try {
            let data = req.body;
            data.updated_at = Date.now();
            const privacypolicy = PrivacyPolicy(data);
            await privacypolicy.save();
            return res.send("Privacy Policy updated successfully");
        } catch (error) {
            console.log(error);
            return res
                .status(500)
                .send("Something went wrong please try again later");
        }
    };
}

module.exports = PrivacyPolicyController;
