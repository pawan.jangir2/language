const Contactus = require("../../models/Contactus");

class ContactusController {
    static contactusGET = async (req, res) => {
        try {
            const data = await Contactus.findOne({});

            return res.render("admin/contactus", {
                content: data ? data.content : "",
            });
        } catch (error) {
            console.log(error);
            return res.send("Something went wrong please try again later");
        }
    };

    static contactusPOST = async (req, res) => {
        try {
            let data = req.body;
            data.updated_at = Date.now();
            const contactus = Contactus(data);
            await contactus.save();
            return res.send("Contact us updated successfully");
        } catch (error) {
            console.log(error);
            return res
                .status(500)
                .send("Something went wrong please try again later");
        }
    };
}

module.exports = ContactusController;
