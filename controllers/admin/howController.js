const How = require("../../models/How");

class HowController {
    static howGET = async (req, res) => {
        try {
            const data = await How.findOne({});

            return res.render("admin/how", {
                content: data ? data.content : "",
            });
        } catch (error) {
            console.log(error);
            return res.send("Something went wrong please try again later");
        }
    };

    static howPOST = async (req, res) => {
        try {
            let data = req.body;
            data.updated_at = Date.now();
            const how = How(data);
            await how.save();
            return res.send("How it Works updated successfully");
        } catch (error) {
            console.log(error);
            return res
                .status(500)
                .send("Something went wrong please try again later");
        }
    };
}

module.exports = HowController;
