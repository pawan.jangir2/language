const Appointment = require("../../models/Appointment");
const moment = require("moment");
const datatablesQuery = require("datatables-query");

class AppointmentController {
    static datatable_data = async (req, res) => {
        try {
            const params = req.body;
            const query = datatablesQuery(Appointment, [
                { path: "patient_user" },
                { path: "doctor" },
            ]);
            const raw = await query.run(params);
            return res.send(raw);
        } catch (error) {
            console.log(error);
            return res
                .status(500)
                .send("Something went wrong please try again later");
        }
    };

    static appointmentsGET = (req, res) => {
        try {
            return res.render("admin/appointment-list");
        } catch (error) {
            return res.send("Something went wrong please try again later");
        }
    };
}

module.exports = AppointmentController;
