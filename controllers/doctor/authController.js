const otpGenerator = require("otp-generator");
const Doctor = require("../../models/Doctor");
const DoctorOtp = require("../../models/DoctorOtp");
require("dotenv").config();
const jwt = require("jsonwebtoken");

class AuthController {
    static login = async (req, res) => {
        const mobile_number = req.body.mobile_number;
        let msg = "Something went wrong please try again later";

        if (!mobile_number) {
            return res.status(401).send(msg);
        }

        try {
            let doctor = await Doctor.findOne({
                mobile_number: mobile_number,
            });

            if (!doctor) {
                return res.status(401).send("Doctor not registered");
            }

            // const newDoctorOtp = otpGenerator.generate(4, {
            //     alphabets: false,
            //     upperCase: false,
            //     specialChars: false,
            // });

            const newDoctorOtp = 1234;

            const DoctorOtpExist = await DoctorOtp.findOne({
                doctor_id: doctor._id,
            });

            if (DoctorOtpExist) {
                await DoctorOtp.findOneAndUpdate(
                    {
                        _id: DoctorOtpExist._id,
                    },
                    {
                        Otp: newDoctorOtp,
                        update_at: Date.now(),
                    }
                );
            } else {
                const doctorOtp = DoctorOtp({
                    doctor: doctor,
                    otp: newDoctorOtp,
                    created_at: Date.now(),
                    update_at: Date.now(),
                });
                await doctorOtp.save();
            }
            return res.send("Otp Sent Successfully");
        } catch (error) {
            console.log(error);
            return res.status(401).send(msg);
        }
    };

    static otp_verify = async (req, res) => {
        let msg = "Something went wrong please try again later";
        try {
            const { mobile_number, otp } = req.body;

            const doctor = await Doctor.findOne({
                mobile_number: mobile_number,
            });

            if (!doctor) return res.status(404).send("Doctor not found");

            const doctorOtp = await DoctorOtp.findOne({ doctor });

            if (otp == doctorOtp.otp) {
                //create and assign a token
                const token = jwt.sign(
                    {
                        _id: doctor._id,
                    },
                    process.env.TOKEN_SECRET
                );
                return res.send(token);
            }

            return res.status(401).send("Invalid Otp");
        } catch (error) {
            console.log(error);
            return res.status(401).send(msg);
        }
    };
}

module.exports = AuthController;
