const Appointment = require("../../models/Appointment");

class AppointmentController {
    static list = async (req, res) => {
        try {
            const data = req.body;
            let finalData = {
                doctor: req.doctor,
                status: 1,
                date_time: {
                    $gte: new Date().setHours(7, 59, 0, 0),
                },
            };

            if (data.last_id) {
                if (data.type == "load") {
                    finalData._id = {
                        $lt: data.last_id,
                    };
                } else {
                    finalData._id = {
                        $gt: data.last_id,
                    };
                }
            }
            let appointments = await Appointment.find(finalData)
                .sort({
                    date_time: 1,
                })
                .populate("patient_user")
                .populate("doctor");

            appointments = await fixData(appointments);
            return res.send(appointments);
        } catch (error) {
            console.log(error);
            return res
                .status(500)
                .send("Something went wrong please try again later");
        }
    };

    static done = async (req, res) => {
        try {
            await Appointment.updateOne(
                {
                    _id: req.body.id,
                    doctor: req.doctor,
                },
                {
                    status: 3,
                }
            );
            return res.send();
        } catch (error) {
            return res
                .status(500)
                .send("Something went wrong please try again later");
        }
    };
}

const fixData = (data) => {
    let finalData = {};
    return new Promise(async (resolve, reject) => {
        for (var i in data) {
            var key = data[i].date_time;
            if (!finalData[key]) finalData[key] = [];
            finalData[key].push(data[i]);
        }
        resolve(finalData);
    });
};

Date.prototype.addDays = function (days) {
    var date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
};

module.exports = AppointmentController;
