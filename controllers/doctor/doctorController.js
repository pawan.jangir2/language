const Doctor = require("../../models/Doctor");

class DoctorController {
    static doctor = async (req, res) => {
        try {
            const doctor = await Doctor.findById(req.doctor._id);
            return res.send(doctor);
        } catch (error) {
            console.log(error);
            return res
                .status(500)
                .send("Something went wrong please try again later");
        }
    };
}

module.exports = DoctorController;
