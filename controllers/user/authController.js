const otpGenerator = require("otp-generator");
const DatingUser = require("../../models/DatingUser");
const Otp = require("../../models/DatingUserOtp");
const Subscription = require("../../models/Subscription");
const FreeVideos = require("../../models/FreeVideos");
const Feedback = require("../../models/Feedback");
const Slider = require("../../models/Slider");
require("dotenv").config();
const jwt = require("jsonwebtoken");
const baseURL = process.env.URL;
const userProfileUrl = baseURL+'dist/influencer_profile';
class AuthController {
    static login = async (req, res) => {
        const mobile_number = req.body.mobile_number;
        let msg = "Something went wrong please try again later";

        var mobile_regex = /^\d{10}$/;

        if (!mobile_regex.test(mobile_number)) {
            return res.status(401).send("Invalid Mobile Number");
        }

        try {
            let dating_user = await DatingUser.findOne({ mobile_number });
            if (!dating_user) {
                dating_user = DatingUser({
                    mobile_number,
                });
                dating_user = await dating_user.save();
            }

            const newOtp = await otpGenerator.generate(4, {
                alphabets: false,
                upperCase: false,
                specialChars: false,
            });

            //const newOtp = 1234;
            let customerMobile = dating_user.mobile_number
            sendSMS(customerMobile,newOtp);

            const otpExist = await Otp.findOne({
                dating_user,
            });

            if (otpExist) {
                await Otp.findOneAndUpdate(
                    {
                        _id: otpExist._id,
                    },
                    {
                        otp: newOtp,
                        update_at: Date.now(),
                    }
                );
            } else {
                const otp = Otp({
                    dating_user,
                    otp: newOtp,
                    created_at: Date.now(),
                    update_at: Date.now(),
                });
                await otp.save();
            }
            return res.send("Otp Sent Successfully");
        } catch (error) {
            console.log(error);
            return res.status(401).send(msg);
        }
    };

    static otp_verify = async (req, res) => {
        let msg = "Something went wrong please try again later";
        try {
            const { mobile_number, otp } = req.body;

            const dating_user = await DatingUser.findOne({ mobile_number });

            if (!dating_user) return res.status(404).send("User not found");

            const userOtp = await Otp.findOne({ dating_user });
            let is_registered = 0
            if(dating_user && dating_user.email && dating_user.email != ''){
                is_registered = 1
            }

            if (otp == userOtp.otp) {
                //create and assign a token
                const token = jwt.sign(
                    {
                        _id: dating_user._id,
                    },
                    process.env.TOKEN_SECRET
                );
                let returnData = {
                    token : token,
                    is_registered : is_registered,
                }
                return res.send(returnData);
            }

            return res.status(401).send("Invalid otp");
        } catch (error) {
            console.log(error);
            return res.status(401).send(msg);
        }
    };

    static check_registered = async (req, res) => {
        let msg = "Something went wrong please try again later";
        try {
            const dating_user = await DatingUser.findById(
                req.dating_user._id
            );
            if (!dating_user.username) return res.send(false);
            res.send(true);
        } catch (error) {
            console.log(error);
            return res.status(401).send(msg);
        }
    };

    static register = async (req, res) => {
        let msg = "Something went wrong please try again later";
        try {
            var token = req.body.token;
            var registerData = req.body.registerData;
            registerData = JSON.parse(registerData)
            const payload = jwt.decode(token, process.env.TOKEN_SECRET);
            const dating_user = await DatingUser.findById(payload._id);
            if (!dating_user) return res.status(401).send("User not found");
            
            //if (dating_user.username == registerData.username)
            //    return res.status(401).send("username already exist");

            var username_regex = /^[a-zA-Z0-9]+$/;
            var email_regex =
                /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
            var name_regex = /^[a-zA-Z ]{2,30}$/;
            var mobile_regex = /^\d{10}$/;

            if (!username_regex.test(registerData.username)) {
                return res.status(500).send("Invalid Username");
            } 
            else if (!email_regex.test(registerData.email)) {
                return res.status(500).send("Invalid Email");
           } 
            else if (!name_regex.test(registerData.name)) {
                return res.status(500).send("Invalid Full Name");
            } 
            // else if (registerData.gender.trim() != "male" || registerData.gender.trim() != "female") 
            // {
            //     console.log()
            //     return res.status(500).send("Invalid Gender");
            // } 
            else if (!registerData.dob) {
                return res.status(500).send("DOB is required");
            }

            await DatingUser.findByIdAndUpdate(dating_user._id, registerData);
            let returnObj = {
                message : 'Success',
                statusCode : 200,
            }
            res.send(returnObj);
        } catch (error) {
            console.log(error);
            return res.status(401).send(msg);
        }
    };
    static user_profile = async (req, res) => {
        let msg = "Something went wrong please try again later";
        try {
            var token = req.body.token;
            
            const payload = jwt.decode(token, process.env.TOKEN_SECRET);
            const dating_user = await DatingUser.findById(payload._id);
            if (!dating_user) return res.status(401).send("User not found");
            
            res.send(dating_user);
        } catch (error) {
            console.log(error);
            return res.status(401).send(msg);
        }
    };
    static subscription_list = async (req, res) => {
        let msg = "Something went wrong please try again later";
        try {
            var token = req.body.token;
            
            const payload = jwt.decode(token, process.env.TOKEN_SECRET);
            const dating_user = await DatingUser.findById(payload._id);
            if (!dating_user) return res.status(401).send("User not found");
            const listData = await Subscription.find();
            res.send(listData);
        } catch (error) {
            console.log(error);
            return res.status(401).send(msg);
        }
    };
    static get_sliders = async (req, res) => {
        let msg = "Something went wrong please try again later";
        try {
            console.log(454)
            var token = req.body.token;
            const userSliderUrl = baseURL+'dist/sliders';
            const payload = jwt.decode(token, process.env.TOKEN_SECRET);
            const dating_user = await DatingUser.findById(payload._id);
            if (!dating_user) return res.status(401).send("User not found");
            const sliders = await Slider.find().sort({
                created_at: -1,
            });
            let returnObj = {
                imagepath : userSliderUrl,
                data : sliders,
            }
            res.send(returnObj);
        } catch (error) {
            console.log(error);
            return res.status(401).send(msg);
        }
    };
    static get_free_videos = async (req, res) => {
        let msg = "Something went wrong please try again later";
        try {
            console.log(454)
            var token = req.body.token;
            const userSliderUrl = baseURL+'dist/free_videos';
            const payload = jwt.decode(token, process.env.TOKEN_SECRET);
            const dating_user = await DatingUser.findById(payload._id);
            if (!dating_user) return res.status(401).send("User not found");
            const sliders = await FreeVideos.find().sort({
                created_at: -1,
            });
            let returnObj = {
                imagepath : userSliderUrl,
                data : sliders,
            }
            res.send(returnObj);
        } catch (error) {
            console.log(error);
            return res.status(401).send(msg);
        }
    };
    static send_feedback = async (req, res) => {
        let msg = "Something went wrong please try again later";
        try {
            let insertObject = {
                name : req.body && req.body.name ? req.body.name : '',
                email : req.body && req.body.email ? req.body.email : '',
                mobile_number : req.body && req.body.mobile_number ? req.body.mobile_number : '',
                feedback : req.body && req.body.feedback ? req.body.feedback : '',
            }
            if(insertObject.name == ''){
                res.status(400).send({
                    status : 400,
                    message : "Please enter name",
                });
            }
            if(insertObject.email == ''){
                res.status(400).send({
                    status : 400,
                    message : "Please enter email",
                });
            }
            if(insertObject.mobile_number == ''){
                res.status(400).send({
                    status : 400,
                    message : "Please enter mobile number",
                });
            }
            if(insertObject.feedback == ''){
                res.status(400).send({
                    status : 400,
                    message : "Please enter feedback",
                });
            }
            await Feedback.create(insertObject);
            // const content = saved ? saved.content : '';
            res.status(200).send({
                status : 200,
                message : "Your details has been saved successfully",
            });
        } catch (error) {
            console.log(error);
            return res.status(401).send(msg);
        }
    };

}
function sendSMS(mobileNo,OTP){
    let theUrl = `http://sms.smsinsta.in/vb/apikey.php?apikey=1158c647754664171463&senderid=SMSINS&templateid=1207162019695960917&route=3&number=${mobileNo}&message=Your OTP is ${OTP}. Regards SMSINSTA`;
    const request = require('request');

    request(theUrl, { json: true }, (err, res, body) => {
    if (err) { return console.log(err); }
        return body
    });
   
}

module.exports = AuthController;
