const Patient = require("../../models/Patient");
require("dotenv").config();

class PatientController {
    static list = async (req, res) => {
        try {
            const data = await Patient.find({
                patient_user: req.patient_user,
            });
            return res.send(data);
        } catch (error) {
            console.log(error);
            return res
                .status(500)
                .send("Something went wrong please try again later");
        }
    };

    static add = async (req, res) => {
        const data = {
            patient_user: req.patient_user,
            patient_name: req.body.patient_name,
        };

        try {
            const duplicate = await Patient.findOne(data);

            if (duplicate) return res.status(500).send("Patient already exist");

            const exist = await Patient.find({
                patient_user: req.patient_user,
            });

            if (exist.length < 5) {
                const patient = Patient(data);
                await patient.save();
                return res.send("success");
            } else {
                return res.status(500).send("You can add maximum 5 patients");
            }
        } catch (error) {
            console.log(error);
            return res
                .status(500)
                .send("Something went wrong please try again later");
        }
    };

    static delete = async (req, res) => {
        try {
            await Patient.findOneAndDelete({
                _id: req.body.id,
                patient_user: req.patient_user,
            });
            return res.send("success");
        } catch (error) {
            console.log(error);
            return res
                .status(500)
                .send("Something went wrong please try again later");
        }
    };
}

module.exports = PatientController;
