const TermsCondition = require("../../models/TermsCondition");

class TermsConditionController {
    static termscondition = async (req, res) => {
        try {
            const data = await TermsCondition.findOne({});
            return res.send(data ? data.content : "");
        } catch (error) {
            console.log(error);
            return res
                .status(500)
                .send("Something went wrong please try again later");
        }
    };
}

module.exports = TermsConditionController;
