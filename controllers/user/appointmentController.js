const Appointment = require("../../models/Appointment");
// const Doctor = require("../../models/Doctor");
// const DoctorCategory = require("../../models/DoctorCategory");

class AppointmentController {
    static list = async (req, res) => {
        try {
            let data = req.body;
            let finalData = {};
            if (data.last_id) {
                if (data.type == "load") {
                    finalData._id = {
                        $lt: data.last_id,
                    };
                } else {
                    finalData._id = {
                        $gt: data.last_id,
                    };
                }
            }
            finalData.patient_user = req.patient_user;
            if (data.appointment_type == "active") {
                (finalData.status = 1),
                    (finalData.date_time = {
                        $gte: Date.now(),
                    });
            } else if (data.appointment_type == "past") {
                {
                    finalData.date_time = { $lt: Date.now() };
                }
            } else {
                finalData.status = 2;
            }
            const appointments = await Appointment.find(finalData)
                .sort({
                    created_at: 1,
                })
                .populate({ path: "doctor", populate: { path: "category" } });
            return res.send(appointments);
        } catch (error) {
            console.log(error);
            return res
                .status(500)
                .send("Something went wrong please try again later");
        }
    };

    static remove = async (req, res) => {
        try {
            await Appointment.findOneAndUpdate(
                {
                    _id: req.data.id,
                    patient_user: req.patient_user,
                },
                {
                    status: 2,
                }
            );
            return res.status(200).send("success");
        } catch (error) {
            console.log(error);
            return res
                .status(500)
                .send("Something went wrong please try again later");
        }
    };
}

module.exports = AppointmentController;
