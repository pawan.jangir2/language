const How = require("../../models/How");

class HowController {
    static how = async (req, res) => {
        try {
            const data = await How.findOne({});
            return res.send(data ? data.content : "");
        } catch (error) {
            console.log(error);
            return res
                .status(500)
                .send("Something went wrong please try again later");
        }
    };
}

module.exports = HowController;
