const DoctorCategory = require("../../models/DoctorCategory");

class DoctorCategoryController {
    static list = async (req, res) => {
        try {
            let data = req.body;
            let finalData = {};
            if (data.last_id) {
                if (data.type == "load") {
                    finalData._id = {
                        $lt: data.last_id,
                    };
                } else {
                    finalData._id = {
                        $gt: data.last_id,
                    };
                }
            }
            finalData.$or = [
                {
                    name: new RegExp(data.query),
                },
            ];
            const categories = await DoctorCategory.find(finalData)
                .sort({
                    created_at: 1,
                })
                .limit(data.skip ? data.skip : 30);
            return res.send(categories);
        } catch (error) {
            console.log(error);
            return res
                .status(500)
                .send("Something went wrong please try agian later");
        }
    };
}

module.exports = DoctorCategoryController;
