const Slider = require("../../models/Slider");

class SliderController {
    static list = async (req, res) => {
        try {
            const sliders = await Slider.find().sort({
                created_at: -1,
            });
            return res.send(sliders);
        } catch (error) {
            return res
                .status(500)
                .send("Something went wrong please try agian later");
        }
    };
}

module.exports = SliderController;
