const Contactus = require("../../models/Contactus");

class ContactusController {
    static contactus = async (req, res) => {
        try {
            const data = await Contactus.findOne({});
            return res.send(data ? data.content : "");
        } catch (error) {
            console.log(error);
            return res
                .status(500)
                .send("Something went wrong please try again later");
        }
    };
}

module.exports = ContactusController;
