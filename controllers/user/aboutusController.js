const Aboutus = require("../../models/Aboutus");

class AboutusController {
    static aboutus = async (req, res) => {
        try {
            const data = await Aboutus.findOne({});
            return res.send(data ? data.content : "");
        } catch (error) {
            console.log(error);
            return res
                .status(500)
                .send("Something went wrong please try again later");
        }
    };
}

module.exports = AboutusController;
