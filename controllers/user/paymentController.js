const Patient = require("../../models/Patient");
const Doctor = require("../../models/Doctor");
const Order = require("../../models/Order");
const Appointment = require("../../models/Appointment");
const Razorpay = require("razorpay");
var invNum = require("invoice-number");
const Receipt = require("../../models/Receipt");

class PaymentController {
    static order_with_rozorpay = async (req, res) => {
        try {
            const data = req.body;
            console.log(data);

            const doctor = await Doctor.findOne({
                _id: data.doctor_id,
            });

            for (var i = 0; i < data.patient_ids.length; i++) {
                var patient = await Patient.findOne({
                    patient_user: req.patient_user,
                    _id: data.patient_ids[i],
                });
                if (!patient) {
                    return res
                        .status(500)
                        .send("Something went wrong please try again later");
                }
            }

            const order_data = await instance.orders.create({
                amount:
                    doctor.consultation_charge * 100 * data.patient_ids.length,
                currency: "INR",
            });

            const order = Order({
                order_id: order_data.id,
                entity: order_data.entity,
                amount: order_data.amount / 100,
                amount_paid: order_data.amount_paid / 100,
                amount_due: order_data.amount_due / 100,
                currency: order_data.currency,
                receipt_id: "12345",
                status: order_data.status,
                attempts: order_data.attempts,
            });
            var savedorder = await order.save();

            const appointment = Appointment({
                order: savedorder,
                patient_user: req.patient_user,
                patients: data.patient_ids,
                doctor: doctor,
                date_time: data.date_time,
                status: 0,
            });
            await appointment.save();

            return res.send(order_data.id);
        } catch (error) {
            console.log(error);
            return res
                .status(500)
                .send("Something went wrong please try again later");
        }
    };

    static checkout_with_rozorpay = async (req, res) => {
        try {
            const data = req.body;

            const payment = await instance.orders.fetch(data.order_id);

            // console.log(payment)

            if (payment.amount_due == 0) {
                return res
                    .status(500)
                    .send("Something went wrong please try again later");
            } else {
                const order = await Order.findOneAndUpdate(
                    {
                        order_id: data.order_id,
                    },
                    {
                        amount_paid: payment.amount_paid,
                        amount_due: payment.amount_due / 100,
                    }
                );

                await Appointment.findOneAndUpdate(
                    {
                        order,
                    },
                    {
                        status: 1,
                    }
                );
                // const receipt = Receipt({

                // })
            }

            return res.send("success");
        } catch (error) {
            console.log(error);
            return res
                .status(500)
                .send("Something went wrong please try again later");
        }
    };
}

var instance = new Razorpay({
    key_id: "rzp_test_IEEFNHtYJSpGWb",
    key_secret: "giQiT99gEr73tZ4vD8j8wv2d",
});

module.exports = PaymentController;
