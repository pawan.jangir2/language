const Doctor = require("../../models/Doctor");
const DoctorCategory = require("../../models/DoctorCategory");

class DoctorController {
    static all_doctors = async (req, res) => {
        try {
            const data = req.body;
            const query = data.query;
            const doctors = await Doctor.find({
                $or: [
                    {
                        doctor_name: new RegExp(query),
                    },
                    {
                        experties: new RegExp(query),
                    },
                    {
                        location: new RegExp(query),
                    },
                ],
            })
                .sort({
                    created_at: 1,
                })
                .populate("category");

            console.log(doctors);
            return res.send(doctors);
        } catch (error) {
            console.log(error);
            return res
                .status(500)
                .send("Something went wrong please try agian later");
        }
    };

    static list = async (req, res) => {
        try {
            let data = req.body;
            let finalData = {};
            if (data.last_id) {
                if (data.type == "load") {
                    finalData._id = {
                        $lt: data.last_id,
                    };
                } else {
                    finalData._id = {
                        $gt: data.last_id,
                    };
                }
            }
            finalData.category_id = data.category_id;
            finalData.$or = [
                {
                    name: new RegExp(data.query.toLowerCase()),
                },
                {
                    experties: new RegExp(data.query.toLowerCase()),
                },
                {
                    location: new RegExp(data.query.toLowerCase()),
                },
            ];
            const doctors = await Doctor.find(finalData)
                .sort({
                    created_at: data.type == "load" ? -1 : 1,
                })
                .limit(30);
            return res.send(doctors);
        } catch (error) {
            console.log(error);
            return res
                .status(500)
                .send("Something went wrong please try agian later");
        }
    };
}

module.exports = DoctorController;
