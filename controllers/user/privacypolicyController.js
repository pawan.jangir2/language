const PrivacyPolicy = require("../../models/PrivacyPolicy");

class PrivacyPolicyController {
    static privacypolicy = async (req, res) => {
        try {
            const data = await PrivacyPolicy.findOne({});
            return res.send(data ? data.content : "");
        } catch (error) {
            console.log(error);
            return res
                .status(500)
                .send("Something went wrong please try again later");
        }
    };
}

module.exports = PrivacyPolicyController;
