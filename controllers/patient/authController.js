const otpGenerator = require("otp-generator");
const PatientUser = require("../../models/PatientUser");
const Otp = require("../../models/PatientUserOtp");
require("dotenv").config();
const jwt = require("jsonwebtoken");

class AuthController {
    static login = async (req, res) => {
        const mobile_number = req.body.mobile_number;
        let msg = "Something went wrong please try again later";

        var mobile_regex = /^\d{10}$/;

        if (!mobile_regex.test(mobile_number)) {
            return res.status(401).send("Invalid Mobile Number");
        }

        try {
            let patient_user = await PatientUser.findOne({ mobile_number });
            if (!patient_user) {
                patient_user = PatientUser({
                    mobile_number,
                });
                patient_user = await patient_user.save();
            }

            // const newOtp = otpGenerator.generate(4, {
            //     alphabets: false,
            //     upperCase: false,
            //     specialChars: false,
            // });

            const newOtp = 1234;

            const otpExist = await Otp.findOne({
                patient_user,
            });

            if (otpExist) {
                await Otp.findOneAndUpdate(
                    {
                        _id: otpExist._id,
                    },
                    {
                        otp: newOtp,
                        update_at: Date.now(),
                    }
                );
            } else {
                const otp = Otp({
                    patient_user,
                    otp: newOtp,
                    created_at: Date.now(),
                    update_at: Date.now(),
                });
                await otp.save();
            }
            return res.send("Otp Sent Successfully");
        } catch (error) {
            console.log(error);
            return res.status(401).send(msg);
        }
    };

    static otp_verify = async (req, res) => {
        let msg = "Something went wrong please try again later";
        try {
            const { mobile_number, otp } = req.body;

            const patient_user = await PatientUser.findOne({ mobile_number });

            if (!patient_user) return res.status(404).send("User not found");

            const userOtp = await Otp.findOne({ patient_user });

            if (otp == userOtp.otp) {
                //create and assign a token
                const token = jwt.sign(
                    {
                        _id: patient_user._id,
                    },
                    process.env.TOKEN_SECRET
                );
                return res.send(token);
            }

            return res.status(401).send("Invalid otp");
        } catch (error) {
            console.log(error);
            return res.status(401).send(msg);
        }
    };

    static check_registered = async (req, res) => {
        let msg = "Something went wrong please try again later";
        try {
            const patient_user = await PatientUser.findById(
                req.patient_user._id
            );
            if (!patient_user.username) return res.send(false);
            res.send(true);
        } catch (error) {
            console.log(error);
            return res.status(401).send(msg);
        }
    };

    static register = async (req, res) => {
        let msg = "Something went wrong please try again later";
        try {
            var token = req.body.token;
            var registerData = req.body.registerData;

            const payload = jwt.decode(token, process.env.TOKEN_SECRET);
            const patient_user = await PatientUser.findById(payload._id);
            if (!patient_user) return res.status(401).send("User not found");
            if (patient_user.username == registerData.username)
                return res.status(401).send("username already exist");

            var username_regex = /^[a-zA-Z0-9]+$/;
            var email_regex =
                /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
            var name_regex = /^[a-zA-Z ]{2,30}$/;
            var mobile_regex = /^\d{10}$/;

            if (!username_regex.test(registerData.username)) {
                return res.status(500).send("Invalid Username");
            } else if (!email_regex.test(registerData.email)) {
                return res.status(500).send("Invalid Email");
            } else if (!name_regex.test(registerData.name)) {
                return res.status(500).send("Invalid Full Name");
            } else if (
                registerData.gender != "male" ||
                registerData.gender != "female"
            ) {
                return res.status(500).send("Invalid Gender");
            } else if (!registerData.dob) {
                return res.status(500).send("DOB is required");
            }

            await PatientUser.findByIdAndUpdate(patient_user._id, registerData);
            res.send();
        } catch (error) {
            console.log(error);
            return res.status(401).send(msg);
        }
    };
}

module.exports = AuthController;
