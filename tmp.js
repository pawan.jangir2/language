const fixData = (data) => {
    const status = {
        0: "Due Payment",
        1: "Active",
        2: "Cancelled",
        3: "Done",
        4: "Past",
    };
    return new Promise(async (resolve, reject) => {
        for (var i in data) {
            try {
                data[i].user_name = data[i].patient_user.username;
                data[i].doctor_name = data[i].doctor.doctor_name;
                if (
                    parseInt(data[i].date_time) < Date.now() &&
                    status[data[i].status] != 0
                ) {
                    data[i].text_status = status[4];
                } else {
                    data[i].text_status = status[data[i].status];
                }
                data[i].date = moment(parseInt(data[i].date_time)).format(
                    "D-MMM"
                );
                data[i].time = moment(parseInt(data[i].date_time)).format(
                    "HH:MM A"
                );
                data[i].created_at = moment(
                    parseInt(data[i].created_at)
                ).format("DD-MMM-YYYY");
                data[i].updated_at = moment(
                    parseInt(data[i].updated_at)
                ).format("DD-MMM-YYYY");
            } catch (error) {
                console.log(error);
            }
        }
        resolve(data);
    });
};
