const mainR = require("./routes/admin/main");
const dashboardR = require("./routes/admin/dashboard");
const authR = require("./routes/admin/auth");
const subscription = require("./routes/admin/subscription");
const languages = require("./routes/admin/languages");
const free_videos = require("./routes/admin/free_videos");
const doctorR = require("./routes/admin/doctor");
const aboutusR = require("./routes/admin/aboutus");
const contactusR = require("./routes/admin/contactus");
const howR = require("./routes/admin/how");
const privacypolicyR = require("./routes/admin/privacypolicy");
const termsconditionR = require("./routes/admin/termscondition");
// const ratingR = require("./routes/patient-app-routes/rating");
const appointmentsR = require("./routes/admin/appoinments");
const systeminfoR = require("./routes/admin/systeminfo");
const sliderR = require("./routes/admin/slider");

const AdminRoutes = (app) => {
    app.use("/", mainR);
    app.use("/admin", dashboardR);
    app.use("/admin", authR);
    app.use("/admin/subscription", subscription);
    app.use("/admin/languages", languages);
    app.use("/admin/free_videos", free_videos);
    app.use("/admin/doctor", doctorR);
    app.use("/admin", aboutusR);
    app.use("/admin", contactusR);
    app.use("/admin", howR);
    app.use("/admin", privacypolicyR);
    app.use("/admin", termsconditionR);
    app.use("/admin/appointment", appointmentsR);
    app.use("/admin", systeminfoR);
    app.use("/admin/slider", sliderR);
};

module.exports = AdminRoutes;
