const router = require("express").Router();
const AuthController = require("../../controllers/doctor/authController");

router.post("/login", AuthController.login);
router.post("/otp-verify", AuthController.otp_verify);

module.exports = router;
