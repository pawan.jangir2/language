const router = require("express").Router();
const { NotLoggedIn } = require("../../middlewares/DoctorAuth");
const AppointmentController = require("../../controllers/doctor/appointmentController");

router.post("/list", NotLoggedIn, AppointmentController.list);
router.post("/done", NotLoggedIn, AppointmentController.done);

module.exports = router;
