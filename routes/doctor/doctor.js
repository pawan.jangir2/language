const router = require("express").Router();
const { NotLoggedIn } = require("../../middlewares/DoctorAuth");
const DoctorController = require("../../controllers/doctor/doctorController");

router.get("/", NotLoggedIn, DoctorController.doctor);

module.exports = router;
