const router = require("express").Router();
const { NotLoggedIn } = require("../../middlewares/Adminauth");
const subscriptionController = require("../../controllers/admin/subscriptionController");

router.post(
    "/datatable-data",
    NotLoggedIn,
    subscriptionController.datatable_data
);
router.get("/list", NotLoggedIn, subscriptionController.list);
router.post("/add", NotLoggedIn, subscriptionController.add);
router.post("/edit", NotLoggedIn, subscriptionController.edit);
router.post("/delete", NotLoggedIn, subscriptionController.delete);
module.exports = router;
