const router = require("express").Router();
const { NotLoggedIn } = require("../../middlewares/Adminauth");
const SystemInfoController = require("../../controllers/admin/systemInfoController");

router.get("/systeminfo", NotLoggedIn, SystemInfoController.systeminfo);

module.exports = router;
