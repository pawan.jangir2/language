const router = require("express").Router();
const { NotLoggedIn } = require("../../middlewares/Adminauth");
const freevideosController = require("../../controllers/admin/freevideosController");

router.post(
    "/datatable-data",
    NotLoggedIn,
    freevideosController.datatable_data
);
router.get("/list", NotLoggedIn, freevideosController.list);
router.post("/add", NotLoggedIn, freevideosController.add);
//router.post("/edit", NotLoggedIn, freevideosController.edit);
router.post("/delete", NotLoggedIn, freevideosController.delete);
module.exports = router;
