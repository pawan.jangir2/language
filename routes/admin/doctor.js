const router = require("express").Router();
const { NotLoggedIn } = require("../../middlewares/Adminauth");
const DoctorController = require("../../controllers/admin/doctorController");

router.post("/datatable-data", NotLoggedIn, DoctorController.datatable_data);
router.get("/list", NotLoggedIn, DoctorController.list);
router.get("/add", NotLoggedIn, DoctorController.addGET);
router.post("/add", NotLoggedIn, DoctorController.addPOST);
router.get("/edit", NotLoggedIn, DoctorController.editGET);
router.post("/edit", NotLoggedIn, DoctorController.editPOST);
router.post("/delete", NotLoggedIn, DoctorController.delete);

module.exports = router;
