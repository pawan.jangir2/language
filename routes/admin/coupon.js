const router = require('express').Router();
const { NotLoggedIn } = require('../../middlewares/Adminauth');
const Coupon = require('../../models/Coupon');
const Aboutus = require('../../models/Coupon');


router.get('/coupon', NotLoggedIn, async (req, res) => {
    try {
        const data = await Coupon.findOne({});

    return res.render('admin/coupon', {content: data ? data.content : ''});
    } catch (error) {
        console.log(error);
        return res.send('Something went wrong please try again later');
    }
});

router.post('/coupon', NotLoggedIn, async (req, res) => {
    const data = req.body;
    try {
        let data = req.body;
        data.updated_at = Date.now();
        coupon = Coupon(data);
        await coupon.save(); 
        return res.send('success');
    } catch (error) {
        console.log(error);
        return res.send('Something went wrong please try again later');
    }
});

module.exports = router;