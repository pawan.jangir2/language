const router = require("express").Router();
const { NotLoggedIn } = require("../../middlewares/Adminauth");
const AppointmentController = require("../../controllers/admin/appointmentController");

router.post(
    "/datatable-data",
    NotLoggedIn,
    AppointmentController.datatable_data
);
router.get("/list", NotLoggedIn, AppointmentController.appointmentsGET);

module.exports = router;
