const router = require("express").Router();
const { NotLoggedIn } = require("../../middlewares/Adminauth");
const languagesController = require("../../controllers/admin/languagesController");

router.post(
    "/datatable-data",
    NotLoggedIn,
    languagesController.datatable_data
);
router.get("/list", NotLoggedIn, languagesController.list);
router.post("/add", NotLoggedIn, languagesController.add);
//router.post("/edit", NotLoggedIn, languagesController.edit);
router.post("/delete", NotLoggedIn, languagesController.delete);
module.exports = router;
