const router = require("express").Router();
const { NotLoggedIn } = require("../../middlewares/Adminauth");
const HowController = require("../../controllers/admin/howController");

router.get("/how", NotLoggedIn, HowController.howGET);
router.post("/how", NotLoggedIn, HowController.howPOST);

module.exports = router;
