const router = require("express").Router();
const { NotLoggedIn } = require("../../middlewares/Adminauth");
const ContactusController = require("../../controllers/admin/contactusController");

router.get("/contactus", NotLoggedIn, ContactusController.contactusGET);
router.post("/contactus", NotLoggedIn, ContactusController.contactusPOST);

module.exports = router;
