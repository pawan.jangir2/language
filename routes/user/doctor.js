const router = require("express").Router();
const { NotLoggedIn } = require("../../middlewares/PatientAuth");
const DoctorController = require("../../controllers/patient/doctorController");

router.post("/all-doctors", NotLoggedIn, DoctorController.all_doctors);
router.post("/list", NotLoggedIn, DoctorController.list);

module.exports = router;
