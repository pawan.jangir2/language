const router = require("express").Router();
const { NotLoggedIn } = require("../../middlewares/PatientAuth");
const AppointmentController = require("../../controllers/patient/appointmentController");

router.post("/list", NotLoggedIn, AppointmentController.list);
router.post("/remove", NotLoggedIn, AppointmentController.remove);

module.exports = router;
