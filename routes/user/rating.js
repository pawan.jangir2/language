const router = require("express").Router();
const Rating = require("../../models/Rating");

router.get("/rating", async (req, res) => {
    try {
        var data = await Rating.find({});
        let average_rating = 0;
        data.forEach(function (item) {
            average_rating += item.rating;
        });
        average_rating /= data.length;
        return res.send(average_rating.toString());
    } catch (error) {
        console.log(error);
        return res
            .status(500)
            .send("Something went wrong please try again later");
    }
});

router.post("/rating", async (req, res) => {
    const data = req.body;
    try {
        // let data = req.body;
        rating = Rating(data);
        await rating.save();
        return res.send("success");
    } catch (error) {
        console.log(error);
        return res.send("Something went wrong please try again later");
    }
});

module.exports = router;
