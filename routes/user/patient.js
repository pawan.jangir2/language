const router = require("express").Router();
const { NotLoggedIn } = require("../../middlewares/PatientAuth");
const PatientController = require("../../controllers/patient/patientController");

router.get("/list", NotLoggedIn, PatientController.list);
router.post("/add", NotLoggedIn, PatientController.add);
router.post("/delete", NotLoggedIn, PatientController.delete);

module.exports = router;
