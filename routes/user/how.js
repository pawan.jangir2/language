const router = require("express").Router();
const { NotLoggedIn } = require("../../middlewares/PatientAuth");
const HowController = require("../../controllers/patient/howController");

router.get("/how", NotLoggedIn, HowController.how);

module.exports = router;
