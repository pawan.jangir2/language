const router = require("express").Router();
const { NotLoggedIn } = require("../../middlewares/PatientAuth");
const SliderController = require("../../controllers/patient/sliderController");

router.get("/list", NotLoggedIn, SliderController.list);

module.exports = router;
