const router = require("express").Router();
const { NotLoggedIn } = require("../../middlewares/PatientAuth");
const AuthController = require("../../controllers/user/authController");

router.post("/login", AuthController.login);
router.post("/otp-verify", AuthController.otp_verify);
router.get("/check-registered", NotLoggedIn, AuthController.check_registered);
router.post("/register", AuthController.register);
router.post("/user_profile", AuthController.user_profile);
router.post("/subscription_list", AuthController.subscription_list);
router.post("/get_sliders", AuthController.get_sliders);
router.post("/get_free_videos", AuthController.get_free_videos);
router.post("/send_feedback", AuthController.send_feedback);

module.exports = router;
``;
