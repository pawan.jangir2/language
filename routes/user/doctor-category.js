const router = require("express").Router();
const { NotLoggedIn } = require("../../middlewares/PatientAuth");
const DoctorCategoryController = require("../../controllers/patient/doctorCategoryController");

router.post("/list", NotLoggedIn, DoctorCategoryController.list);

module.exports = router;
