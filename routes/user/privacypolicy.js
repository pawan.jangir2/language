const router = require("express").Router();
const { NotLoggedIn } = require("../../middlewares/PatientAuth");
const PrivacyPolicyController = require("../../controllers/patient/privacypolicyController");

router.get(
    "/privacypolicy",
    NotLoggedIn,
    PrivacyPolicyController.privacypolicy
);

module.exports = router;
