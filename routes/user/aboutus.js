const router = require("express").Router();
const { NotLoggedIn } = require("../../middlewares/PatientAuth");
const AboutusController = require("../../controllers/patient/aboutusController");

router.get("/aboutus", NotLoggedIn, AboutusController.aboutus);

module.exports = router;
