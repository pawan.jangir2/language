const router = require("express").Router();
const { NotLoggedIn } = require("../../middlewares/PatientAuth");
const TermsConditionController = require("../../controllers/patient/termsconditionController");

router.get(
    "/termscondition",
    NotLoggedIn,
    TermsConditionController.termscondition
);

module.exports = router;
