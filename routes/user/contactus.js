const router = require("express").Router();
const { NotLoggedIn } = require("../../middlewares/PatientAuth");
const ContactusController = require("../../controllers/patient/contactusController");

router.get("/contactus", NotLoggedIn, ContactusController.contactus);

module.exports = router;
