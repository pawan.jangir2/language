const authR = require("./routes/user/auth");
//const doctorcategoryR = require("./routes/user/doctor-category");
//const doctorR = require("./routes/user/doctor");
// const patientR = require("./routes/user/patient");
// const helpR = require("./routes/user/help");
// const paymentR = require("./routes/user/payment");
// const appointmentR = require("./routes/user/appointment");
// const aboutusR = require("./routes/user/aboutus");
// const contactusR = require("./routes/user/contactus");
// const howR = require("./routes/user/how");
// const privacypolicyR = require("./routes/user/privacypolicy");
// const termsconditionR = require("./routes/user/termscondition");
// const sliderR = require("./routes/user/slider");

const UserRoutes = (app) => {
    app.use("/language-app/auth", authR);
    app.use("/language-app/user", authR);
    // app.use("/dating-app/doctor-category", doctorcategoryR);
    // app.use("/dating-app/doctor", doctorR);
    // // app.use("/patient-app", admin_ratingR);
    // app.use("/dating-app/patient", patientR);
    // app.use("/dating-app", helpR);
    // app.use("/dating-app/payment", paymentR);
    // app.use("/dating-app/appointment", appointmentR);
    // app.use("/dating-app", aboutusR);
    // app.use("/dating-app", contactusR);
    // app.use("/dating-app", howR);
    // app.use("/dating-app", privacypolicyR);
    // app.use("/dating-app", termsconditionR);
    // app.use("/dating-app/slider", sliderR);
};

module.exports = UserRoutes;
