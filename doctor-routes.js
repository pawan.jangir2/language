const authR = require("./routes/doctor/auth");
const appointmentR = require("./routes/doctor/appointment");
const doctorR = require("./routes/doctor/doctor");

const DoctorRoutes = (app) => {
    app.use("/doctor-app/auth", authR);
    app.use("/doctor-app/appointment", appointmentR);
    app.use("/doctor-app/doctor", doctorR);
};

module.exports = DoctorRoutes;
