const mongoose = require('mongoose');

const schema = new mongoose.Schema({
    rating: {
        type: Number,
        require: true,
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        require: true,
    },
    doctor: {
        type: mongoose.Schema.Types.ObjectId,
        require: true,
    },
    created_at: {
        type: String,
        default: Date.now
    },
    updated_at: {
        type: String,
        default: Date.now
    },
});

module.exports = mongoose.model('Rating', schema);