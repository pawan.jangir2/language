const mongoose = require('mongoose');

const schema = new mongoose.Schema({
    month: {
        type: Number,
        required: true
    }, 
    sessions: {
        type: Number,
        required: true
    }, 
    amount: {
        type: Number,
        required: true
    }, 
    original_amount: {
        type: Number,
        required: true
    }, 
    created_at: {
        type: String,
        default: Date.now
    },
    updated_at: {
        type: String,
        default: Date.now
    },
});

module.exports = mongoose.model('Subscription', schema);