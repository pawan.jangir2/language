const mongoose = require('mongoose');

const FreeVideosSchema = new mongoose.Schema({
   
    name: {
        type: String,
        required: true
    }, 
    link: {
        type: String,
        //required: true
    }, 
    icon: {
        type: String,
        //required: true
    }, 
    description: {
        type: String,
        //required: true
    },
    created_at: {
        type: String,
        default: Date.now(),
    },
});

module.exports = mongoose.model('free_videos', FreeVideosSchema);