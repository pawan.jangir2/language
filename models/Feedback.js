const mongoose = require('mongoose');

const enquirySchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    }, 
    email: {
        type: String,
        required: true
    }, 
    mobile_number: {
        type: String,
        required: true
    }, 
    feedback: {
        type: String,
        required: true
    }, 
    last_update_time: {
        type: Date,
        default: Date.now
    },
    created_at: {
        type: Date,
        default: Date.now
    },
});

module.exports = mongoose.model('Feedback', enquirySchema);
