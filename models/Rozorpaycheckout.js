const mongoose = require('mongoose');

const schema = new mongoose.Schema({
    payment_id: {
        type: String,
        require: true,
    },
    order_id: {
        type: String,
        require: true,
    },
    signature: {
        type: String,
        require: true,
    },
    created_at: {
        type: String,
        default: Date.now
    },
    updated_at: {
        type: String,
        default: Date.now
    },
});

module.exports = mongoose.model('Rozorpaycheckout', schema);