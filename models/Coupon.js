var mongoose = require('mongoose');
var Coupon = mongoose.Schema({
    code: {
        type: String,
        require: true,
        unique: true
    },
    isPercent: {
        type: Boolean,
        require: true,
        default: true
    },
    amount: {
        type: Number,
        required: true
    },
    discountexpireDate: {
        type: String,
        require: true, 
        default: ''
    },
    isActive: {
        type: Boolean,
        require: true,
        default: true
    }
});

module.exports = Coupon;