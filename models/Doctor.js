const mongoose = require("mongoose");

const schema = new mongoose.Schema({
    image: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
    },
    mobile_number: {
        type: String,
        required: true,
    },
    category: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "DoctorCategory",
        required: true,
    },
    name: {
        type: String,
        required: true,
    },
    experience: {
        type: String,
        required: true,
    },
    consultation_charge: {
        type: String,
        required: true,
    },
    open_time: {
        type: String,
        required: true,
    },
    close_time: {
        type: String,
        required: true,
    },
    location: {
        type: String,
        required: true,
    },
    language: {
        type: String,
        required: true,
    },
    experties: {
        type: String,
        required: true,
    },
    account_holder: {
        type: String,
        required: true,
    },
    bank_name: {
        type: String,
        required: true,
    },
    ifsc: {
        type: String,
        required: true,
    },
    account_number: {
        type: String,
        required: true,
    },
    upi: {
        type: String,
        required: true,
    },
    created_at: {
        type: String,
        default: Date.now,
    },
    updated_at: {
        type: String,
        default: Date.now,
    },
});

module.exports = mongoose.model("Doctor", schema);
