const mongoose = require('mongoose');

const schema = new mongoose.Schema({
    dating_user: {
        type: mongoose.Schema.Types.ObjectId,
        require: true,
    },
    user_name: {
        type: String,
        require: true,
    },
    created_at: {
        type: String,
        default: Date.now
    },
    updated_at: {
        type: String,
        default: Date.now
    },
});

module.exports = mongoose.model('User', schema);