const mongoose = require("mongoose");

const schema = new mongoose.Schema({
    dating_user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "DatingUser",
        max: 255,
        required: true,
    },
    otp: {
        type: String,
        max: 5,
        required: true,
    },
    created_at: {
        type: String,
        default: Date.now,
    },
    updated_at: {
        type: String,
        default: Date.now,
    },
});

module.exports = mongoose.model("DatingUserOtp", schema);
