const mongoose = require("mongoose");

const schema = new mongoose.Schema({
    username: {
        type: String,
        min: 5,
        max: 10,
    },
    mobile_number: {
        unique: true,
        type: String,
        required: true,
    },
    name: {
        type: String,
        min: 4,
        max: 30,
        // required: true
    },
    image: {
        type: String,
        max: 255,
    },
    email: {
        type: String,
        max: 255,
    },
    age: {
        type: String,
        max: 255,
    },
    gender: {
        type: String,
        max: 255,
    },
    dob: {
        type: String,
        max: 255,
    },
    created_at: {
        type: String,
        default: Date.now,
    },
    updated_at: {
        type: String,
        default: Date.now,
    },
});

module.exports = mongoose.model("DatingUser", schema);
