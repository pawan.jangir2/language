const mongoose = require('mongoose');

const schema = new mongoose.Schema({
    order_id: {
        type: String,
        require: true,
    },
    entity: {
        type: String,
        require: true,
        default: 'order'
    },
    amount: {
        type: Number,
        require: true,
    },
    amount_paid: {
        type: Number,
        require: true,
    },
    amount_due: {
        type: Number,
        require: true,
    },
    currency: {
        type: String,
        require: true,
        default: 'INR'
    },
    receipt_id: {
        type: String,
        require: true,
    },
    status: {
        type: String,
        require: true,
        default: 'created'
    },
    attempts: {
        type: Number,
        require: true,
        default: 0
    },
    created_at: {
        type: String,
        default: Date.now
    },
    updated_at: {
        type: String,
        default: Date.now
    },
});

module.exports = mongoose.model('Order', schema);