const mongoose = require("mongoose");

const schema = new mongoose.Schema({
    order: {
        type: mongoose.Schema.Types.ObjectId,
        require: true,
    },
    patient_user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "PatientUser",
        require: true,
    },
    patients: {
        type: [mongoose.Schema.Types.ObjectId],
        require: true,
    },
    doctor: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Doctor",
        require: true,
    },
    date_time: {
        type: String,
        require: true,
    },
    status: {
        type: Number,
        require: true,
        default: 0,
    },
    created_at: {
        type: String,
        default: Date.now,
    },
    updated_at: {
        type: String,
        default: Date.now,
    },
});

module.exports = mongoose.model("Appointment", schema);
