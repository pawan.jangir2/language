const mongoose = require('mongoose');

const languageSchema = new mongoose.Schema({
   
    name: {
        type: String,
        required: true
    }, 
    description: {
        type: String,
        //required: true
    }, 
    created_at: {
        type: String,
        default: Date.now(),
    },
});

module.exports = mongoose.model('languages', languageSchema);