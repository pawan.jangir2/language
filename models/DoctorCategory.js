const mongoose = require('mongoose');

const schema = new mongoose.Schema({
    name: {
        unique: true,
        type: String,
        min: 5,
        max: 50,
        required: true
    }, 
    icon: {
        unique: true,
        type: String,
        max: 500,
        required: true
    }, 
    created_at: {
        type: String,
        default: Date.now
    },
    updated_at: {
        type: String,
        default: Date.now
    },
});

module.exports = mongoose.model('DoctorCategory', schema);
